'use strict'

var Topic = require('../models/topic');
var jwt = require('../services/jwt');
var validator = require('validator');

var controller = {

    test: function (req, res) {
        return res.status(200).send({ message: 'test desde topic' });
    },

    //Guardar tema nuevo
    save: function (req, res) {
        //recoger pàrámetros:
        var params = req.body;

        //validar datos
        try {
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.title);
            var validate_lang = !validator.isEmpty(params.lang);
        } catch (err) {
            return res.status(200).send({ message: 'Faltan datos' });
        }
        if (validate_title && validate_content && validate_lang) {
            //si es correcta la validación, crear nuevo topic y asignarle los campos:
            var topic = new Topic();
            topic.title = params.title;
            topic.content = params.content;
            topic.lang = params.lang;
            topic.code = params.code;
            topic.user = req.user.sub;
            //guardar topic
            topic.save((err, topicStored) => {
                if (err || !topicStored) {
                    return res.status(500).send({ message: 'Error al guardar el topic', status: 'error' });
                } else {
                    return res.status(200).send({ message: 'Topic guardado con éxito', status: 'success', topic: topicStored });
                }
            });

        } else {
            return res.status(200).send({ message: 'Los datos no son válidos' });
        }
    },

    //sacar todos los topics
    getTopics: function (req, res) {
        //recoger página

        if (req.params.page == null || req.params.page == undefined || req.params.page == 0) {
            page = 1;
        } else {
            var page = req.params.page;
        }

        //opciones de paginación:
        var options = {
            sort: { date: -1 },
            populate: 'user',
            limit: 5,
            page: page,
        }

        Topic.paginate({}, options, (err, result) => {
            if (err) return res.status(500).send({ message: 'Error al paginar los topics', status: 'error' });
            if (!result) return res.status(404).send({ message: 'No hay topics', status: 'notfound' });
            return res.status(200).send(
                {
                    status: 'success',
                    topics: result.docs,
                    total: result.totalDocs,
                    page: page,
                    pages: result.totalPages,
                    prevPage: result.prevPage,
                    nextPage: result.nextPage,
                }
            );
        });
    },

    //sacar topics por usuario
    getTopicsByUser: function (req, res) {
        //recoger usuario que llega por la cabecera
        var userId = req.params.user;

        //consulta:
        Topic.find({ user: userId }).sort({ date: 'desc' }).exec((err, topics) => {
            if (err) return res.status(500).send({ message: 'Error al buscar topics', status: 'error' });
            if (topics.length <= 0) {
                return res.status(404).send({ message: 'No hay topics de este usuario', status: 'notfound' });
            } else {
                return res.status(200).send({ status: 'success', topics: topics });
            }
        });
    },

    //Detalles del topic
    getTopic: function (req, res) {
        var id = req.params.id;
        Topic.findById(id).populate('user').exec((err, topic) => {
            if (err) return res.status(500).send({ message: 'Error al buscar tópico', status: 'error' });
            if (!topic) {
                return res.status(404).send({ message: 'No existe el topic', status: 'error' });
            } else {
                topic.user.password = undefined;
                return res.status(200).send({ status: 'success', topic: topic });
            }
        });
    },

    //actualizar topic
    updateTopic: function (req, res) {
        //recoger id topic a actualizar
        var topicId = req.params.id;
        //recoger parámetros
        var params = req.body;

        //validar 
        try {
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);
            var validate_lang = !validator.isEmpty(params.lang);
        } catch (err) {
            return res.status(500).send({ message: 'Faltan datos' });
        }
        if (validate_title && validate_content && validate_lang) {

            var update = {
                title: params.title,
                content: params.content,
                code: params.code,
                lang: params.lang,
            }

            Topic.findOneAndUpdate({ _id: topicId, user: req.user.sub }, update, { new: true }, (error, topicUpdated) => {
                if (error) return res.status(500).send({ message: 'Error al actualizar topic', status: 'error' });
                if (!topicUpdated) {
                    return res.status(404).send({ message: 'El topic no se ha actualizado', status: 'error' });
                } else {
                    return res.status(200).send({ message: 'Topic actualizado con éxito', status: 'success', topic: topicUpdated });
                }
            });
        } else {
            return res.status(500).send({ message: 'Los datos no son válidos' });
        }

    },

    deleteTopic: function (req, res) {
        //recoger id topic
        var topicId = req.params.id;

        Topic.findOneAndRemove({ _id: topicId, user: req.user.sub }, (error, deletedTopic) => {
            if (error) return res.status(500).send({ message: 'Error al borrar topic', status: 'error' });

            if (!deletedTopic) return res.status(404).send({ message: 'Error no existe el topic', status: 'error' });

            return res.status(200).send({ message: 'Topic borrado con éxito', status: 'success', topic: deletedTopic });
        });
    },

    search: function (req, res) {
        //recoger cadena de búsqueda
        var searchString = req.params.search;

        Topic.find({
            '$or': [
                { title: { '$regex': searchString, '$options': 'i' } },
                { content: { '$regex': searchString, '$options': 'i' } },
                { code: { '$regex': searchString, '$options': 'i' } },
                { lang: { '$regex': searchString, '$options': 'i' } },
            ]
        })
        .sort({date:'desc'})
        .exec((err,topics)=>{
            if(err) return res.status(500).send({ message: 'Error en la petición', status: 'error' });

            if(!topics) return res.status(404).send({ message: 'No hay coincidencias', status: 'notfound' });

            return res.status(200).send({status: 'success',topics });
        });


    }

}

module.exports = controller;
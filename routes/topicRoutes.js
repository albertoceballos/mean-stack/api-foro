'use strict'

var express=require('express');
var router=express.Router();

//middlewares
var md_auth=require('../middlewares/authenticated');

//controlador
var topicController=require('../controllers/topicController');

//rutas

router.get('/test-topic',topicController.test);
router.post('/topic',md_auth.auth,topicController.save);
router.get('/topics/:page?',topicController.getTopics);
router.get('/user-topics/:user',topicController.getTopicsByUser);
router.get('/topic/:id',topicController.getTopic);
router.put('/topic/:id',md_auth.auth,topicController.updateTopic);
router.delete('/topic/:id',md_auth.auth,topicController.deleteTopic);
router.get('/search/:search',topicController.search);

module.exports=router;
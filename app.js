'use strict'

//requires
var express=require('express');
var bodyParser=require('body-parser');


//ejecutar Express
var app=express();


//Middleware
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


var user_routes=require('./routes/userRoutes');
var topic_routes=require('./routes/topicRoutes');
var comment_routes=require('./routes/commentRoutes');


// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
//


app.use('/api',user_routes);
app.use('/api',topic_routes);
app.use('/api',comment_routes);

module.exports=app;


var express=require('express');
var router=express.Router();

var md_auth=require('../middlewares/authenticated');

var multiparty=require('connect-multiparty');
var md_upload=multiparty({uploadDir:'./uploads/users'});

var userController=require('../controllers/userController');

router.get('/probando',userController.probando);
router.post('/save',userController.save);
router.post('/login',userController.login);
router.put('/update',md_auth.auth, userController.update);
router.post('/upload-avatar/',[md_auth.auth,md_upload],userController.uploadAvatar);
router.get('/avatar/:fileName',userController.getAvatar);
router.get('/users',md_auth.auth,userController.getUsers);
router.get('/user/:userId',md_auth.auth,userController.getUser);

module.exports=router;

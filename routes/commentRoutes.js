'use strict'

var express=require('express');
var router=express.Router();

//middleware
var md_auth=require('../middlewares/authenticated');

var comment_controller=require('../controllers/commentController');


router.get('/test-comments',comment_controller.test);
router.post('/comment/topic/:id',md_auth.auth,comment_controller.add);
router.put('/comment/:id',md_auth.auth,comment_controller.update);
router.delete('/comment/:topicId/:commentId',md_auth.auth,comment_controller.delete);


module.exports=router;